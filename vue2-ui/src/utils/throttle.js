export default function throttle(fn, wait) {
	let time = null;
	return function () {
		const nowTime = Date.now();
		let args = arguments;
		if (nowTime - time >= wait) {
			fn.apply(this, args);
			time = nowTime;
		}
	};
}
