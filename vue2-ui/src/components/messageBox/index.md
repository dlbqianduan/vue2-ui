## 全局引入
import messageBox from '@/components/messageBox/index.js'; //根据自己文件位置修改
Vue.prototype.$messageBox = messageBox;

## options
title:  消息弹窗标题（string）

message: 消息内容（string）

type:    消息类型（string） 可选值：info | warning | error | success

dangerouslyUseHTMLString: 是否将message属性作为HTML片段处理(boolean)

callback: 消息弹窗关闭后的回调（function(action, instance)，action 的值为'confirm'或'cancel或close', instance 为 MessageBox 实例，可以通过它访问实例上的属性和方法）

showClose：是否显示关闭按钮（boolean）

lockScroll: 是否在消息弹窗出现时将body滚动锁定（boolean）

beforeClose: 消息弹窗关闭前的回调，会暂停当前弹窗的关闭（function(action, instance, done)，action 的值为'confirm'或'cancel'或'close'；instance 为 MessageBox 实例，可以通过它访问实例上的属性和方法；done 用于关闭 MessageBox 实例）

showCancelButton: 是否显示取消按钮（boolean）

showConfirmButton: 是否显示确定按钮（boolean）

cancelButtonText: 取消按钮文本内容（string）

confirmButtonText: 确定按钮文本内容（string）

closeOnClickModal:  是否可通过点击遮罩关闭消息弹窗（boolean）

closeOnPressEscape: 是否可通过按下 ESC 键关闭 消息弹窗（boolean）

closeOnHashChange: 是否在 hashchange 时关闭 消息弹窗（boolean）

