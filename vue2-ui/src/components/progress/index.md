## 属性

percentage: 百分比进度 （number）

type: 进度条类型 (string)

stroke-width: 进度条的宽度，单位 px (number)

text-inside: 进度条显示文字内置在进度条内（只在 type=line 时可用）

status: 进度条当前状态 (string：success/exception/warning)

color: 进度条背景色（会覆盖 status 状态颜色）；string/function

width: 环形进度条画布宽度（只在 type 为 circle 或 dashboard 时可用）。（number）

show-text: 是否显示进度条文字内容。（boolean）

stroke-linecap: type 为 circle/dashboard 类型时路径两端的形状。（string: butt/round/square）

format: 指定进度条文字内容（function(percentage)）
