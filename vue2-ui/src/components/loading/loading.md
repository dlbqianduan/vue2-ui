## 全局引入

import loading from '@/components/loading/loading.js'; //根据自己文件位置修改
Vue.prototype.$loading = loading;

## options

target: Loading 组件需要插入的 DOM 节点

background: 遮罩层背景色

text: 显示在加载图标下方的加载文案

textColor: 加载文案字体颜色

autoClose：是否开启自动关闭，默认不开启

autoCloseDuration：自动关闭时长，单位 ms。默认 8000ms。
