## 属性

action: 必传参数，上传的地址(string)

name: 上传文件字段名(string)

data: 上传时附带的额外参数(object)

headers: 设置请求头(object)

withCredentials: 是否支持发送 cookie 凭证信息(boolean)

accept: 接受的文件类型(https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attr-accept)

## 事件

beforeUpload: 上传文件之前的钩子，参数为上传的文件，支持返回 boolean 值、Promise 对象。若返回 false 或者返回 Promise 且被 reject，则停止上传。(function(file))

onProgress: 文件上传中的钩子(function(event, file, fileList))

onSuccess: 文件上传成功时的钩子(function(event, file, fileList))

onError: 文件上传失败时的钩子(function(event, file, fileList))
