## 全局引入
import message from '@/components/message/index.js'; //根据自己文件位置修改
Vue.prototype.$message = message;

## options

message: 消息内容（string）

type:    消息类型（string） 可选值：info | warning | error | success

dangerouslyUseHTMLString: 是否将message属性作为HTML片段处理(boolean)

duration: 显示时间,单位毫秒 (number)。设为 0 则不会自动关闭

showClose：是否显示关闭按钮（boolean）

center:  文字是否居中（boolean）

offset:  消息内容距离顶部的距离（number）

onClose: 关闭时的回调函数, 参数为被关闭的 message 实例（function）
