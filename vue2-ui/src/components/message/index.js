import Vue from "vue";
import Index from "./index.vue";
let MessageConstructor = Vue.extend(Index);
let instance;
let instances = [];
let seed = 1;
const Message = (option = {}) => {
  const _default_option = {
    type: "info",
    message: "这是一条信息!",
    duration: 3000,
  };
  const _option = Object.assign(_default_option, option);
  let userOnClose = _option.onClose;
  let id = "message_" + seed++;

  _option.onClose = function () {
    Message.close(id, userOnClose);
  };

  instance = new MessageConstructor({
    data: _option,
  });
  instance.id = id;

  if (isVNode(instance.message)) {
    instance.$slots.default = [instance.message];
    instance.message = null;
  }
  instance.$mount();

  document.body.appendChild(instance.$el);

  let verticalOffset = _option.offset || 20;
  instances.forEach((item) => {
    verticalOffset += item.$el.offsetHeight + 16;
  });

  instance.verticalOffset = verticalOffset;
  instance.visible = true;

  instances.push(instance);
  return instance;
};

Message.close = function (id, userOnClose) {
  let len = instances.length;
  let index = -1;
  let removedHeight;
  for (let i = 0; i < len; i++) {
    if (id === instances[i].id) {
      removedHeight = instances[i].$el.offsetHeight;
      index = i;
      if (typeof userOnClose === "function") {
        userOnClose(instances[i]);
      }
      instances.splice(i, 1);
      break;
    }
  }
  if (len <= 1 || index === -1 || index > instances.length - 1) return;
  for (let i = index; i < len - 1; i++) {
    let dom = instances[i].$el;
    dom.style["top"] =
      parseInt(dom.style["top"], 10) - removedHeight - 16 + "px";
      console.log(dom.style['top'])
  }
};

Message.closeAll = function () {
  for (let i = instances.length - 1; i >= 0; i--) {
    instances[i].close();
  }
};

function isVNode(node) {
  return (
    node !== null &&
    typeof node === "object" &&
    hasOwn(node, "componentOptions")
  );
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

export default Message;
