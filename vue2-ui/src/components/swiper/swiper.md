## 属性

height: swiper 的高度(string)

initialIndex: 初始状态激活的幻灯片的索引，从 0 开始

autoplay: 是否自动切换(boolean)

interval: 自动切换的时间间隔，单位为毫秒

loop: 是否循环显示

## 事件

change: 幻灯片切换时触发。参数：目前激活的幻灯片的索引，原幻灯片的索引
