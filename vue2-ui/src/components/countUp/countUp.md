## 属性

value：数值(numbe)

startValue: 数字动画开始值(number)

time: 数字动画持续时间(number)(毫秒)

useease：是否开启 easeout 动画效果(boolean)

tofixed：数值保留小数位(number)

separator：是否显示数值分隔符(boolean)

reset：只用作测试时使用

## 事件

end: 数字动画结束后调用
