## 属性

value：图形验证码内容（由后端提供时填写）

nums: 验证码长度(number)，默认：4

width: 图形验证码宽度(number)

height: 图形验证码高度(number)

color: 图形验证码内容颜色(string)

fillStyle：图形验证码填充颜色(string)

randomLine：是否开启随机干扰线条(boolean)

clickUpdate：是否开启点击更新图形验证码(boolean)

## 事件

getImgCode: 获取图形验证码内容
