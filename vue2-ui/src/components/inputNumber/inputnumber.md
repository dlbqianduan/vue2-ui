## 属性

v-model:  双向数据绑定 (number)

placeHolder：输入框占位 (number)

disabled：是否禁用计数器 (boolean)

step: 计数器步长(每次增加或减少的数)

precision：数值精度（保留位数）

max：计数器最大值（v-model最大值）

min: 计数器最小值（v-model最小值）

## 事件

focus:  在 Input 获得焦点时触发  (event: Event)

blur:   在 Input 失去焦点时触发   (event: Event)

input:  在 Input 值改变时触发     (value: number)

change: 仅在输入框失去焦点或用户按下回车时触发  (value: number)