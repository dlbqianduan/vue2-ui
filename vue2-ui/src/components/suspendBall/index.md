## 属性

dragableRange: 拖动范围。parent 父级 window 可视窗口

dragable: 是否可拖动。默认 true

position：初始位置。[object Object] top、left、right、bottom

indent：是否需要缩进。默认 false

indentDelayTime：延时缩进。单位：ms, 为 0 则不缩进

indentDistance：缩进距离。单位：px

needNearEdge：拖动悬浮球后是否需要贴边。默认：false

nearEdgeTransition: 贴边过渡动画，transition 属性值。默认：'all ease 0.3s'

nearEdgeDirection：拖动悬浮球后贴边方向。默认贴边方向为距离最近的方向。

indentNearEdge：悬浮球贴边后是否需要缩进（此时缩进方向为贴边的方向）。默认 false

indentNearEdgeDelay: 悬浮球贴边后延时缩进。单位：ms，默认 1000，为 0 则不延时

## 事件

clickFunc：点击悬浮球后回调事件

## slots

slotsDirection：插槽内容方向，默认置于悬浮球右边（如不设置且已开启 needNearEdge，则自动根据贴边情况来改变方向）
