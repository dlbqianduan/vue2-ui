## 属性

v-model: 双向数据绑定值(boolean/string/number)

disabled: 是否禁用switch(boolean)

width: switch的宽度(px)

activeValue: switch打开时的值(boolean/string/number)

inactiveValue: switch关闭时的值(boolean/string/number)

activeText: switch打开时的文字描述(string)

inactiveText: switch关闭时的文字描述(string)

activeColor: switch打开时的背景颜色

inactiveColor: switch关闭时的背景颜色

name: switch的name属性(string)

## 事件

change: 	switch 状态发生变化时触发(val：当前开关状态)