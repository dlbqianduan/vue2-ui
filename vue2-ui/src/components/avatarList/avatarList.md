## 属性

value：头像列表

disabled：是否禁用avatar

imgAttr：头像属性

showHoverText：是否显示头像hover信息

hovertextAttr：头像hover内容属性

max：头像最多显示数量

## 事件

click: 点击avatar触发（val:当前avatar信息）