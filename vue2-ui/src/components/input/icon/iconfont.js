!(function (e) {
  var t,
    n,
    o,
    i,
    c,
    d,
    a =
      '<svg><symbol id="v-iconqingchu" viewBox="0 0 1024 1024"><path d="M737.2 307C624 193.7 440.2 193.7 327 307s-113.3 297 0 410.3 297 113.3 410.3 0 113.3-297-0.1-410.3zM363.1 681.1c-93.3-93.3-93.3-244.6 0-337.9 93.3-93.3 244.6-93.3 337.9 0 93.3 93.3 93.3 244.6 0 337.9-93.2 93.3-244.6 93.3-337.9 0z"  ></path><path d="M658.8 421.6l-90.5 90.5 90.5 90.5-36.2 36.2-90.5-90.5-90.5 90.5-36.2-36.2 90.5-90.5-90.5-90.5 36.2-36.2 90.5 90.5 90.5-90.5z"  ></path></symbol><symbol id="v-iconchakan" viewBox="0 0 1024 1024"><path d="M512.3 195.4c-247.5 0-448.1 217.3-448.1 318.2 0 98.2 200.6 318.2 448.1 318.2s448.1-221.9 448.1-318.2c0-99.9-200.7-318.2-448.1-318.2z m275.6 463.8C749.5 691.1 706 717.7 662 736c-49.8 20.7-100.1 31.2-149.6 31.2-49.6 0-100-10.4-149.8-31-44.1-18.2-87.6-44.6-125.9-76.4-33.7-27.9-63.3-60.1-83.3-90.6-19.4-29.5-24.6-49.3-24.6-55.7 0-7.2 5.2-27.3 24.6-56.9 20-30.4 49.5-62.5 83.2-90.3 38.2-31.5 81.7-57.7 125.8-75.8 49.8-20.4 100.3-30.7 150-30.7s100.1 10.4 149.9 30.8c44.1 18.1 87.6 44.4 125.8 76 33.7 27.8 63.2 59.9 83.2 90.4 19.4 29.6 24.6 49.4 24.6 56.5 0 6.1-5.1 25.4-24.5 54.8-20.2 30.6-49.9 62.9-83.5 90.9z"  ></path><path d="M510.9 321c-106 0-191.9 85.9-191.9 191.9s85.9 191.9 191.9 191.9 191.9-85.9 191.9-191.9S616.9 321 510.9 321z m82 273.9c-21.9 21.9-51 34-82 34s-60.1-12.1-82-34-34-51-34-82 12.1-60.1 34-82 51-34 82-34 60.1 12.1 82 34 34 51 34 82-12.1 60.1-34 82z"  ></path></symbol></svg>',
    s = (s = document.getElementsByTagName("script"))[
      s.length - 1
    ].getAttribute("data-injectcss");
  if (s && !e.__iconfont__svg__cssinject__) {
    e.__iconfont__svg__cssinject__ = !0;
    try {
      document.write(
        "<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>"
      );
    } catch (e) {
      console && console.log(e);
    }
  }
  function l() {
    c || ((c = !0), o());
  }
  (t = function () {
    var e, t, n, o;
    ((o = document.createElement("div")).innerHTML = a),
      (a = null),
      (n = o.getElementsByTagName("svg")[0]) &&
        (n.setAttribute("aria-hidden", "true"),
        (n.style.position = "absolute"),
        (n.style.width = 0),
        (n.style.height = 0),
        (n.style.overflow = "hidden"),
        (e = n),
        (t = document.body).firstChild
          ? ((o = e), (n = t.firstChild).parentNode.insertBefore(o, n))
          : t.appendChild(e));
  }),
    document.addEventListener
      ? ~["complete", "loaded", "interactive"].indexOf(document.readyState)
        ? setTimeout(t, 0)
        : ((n = function () {
            document.removeEventListener("DOMContentLoaded", n, !1), t();
          }),
          document.addEventListener("DOMContentLoaded", n, !1))
      : document.attachEvent &&
        ((o = t),
        (i = e.document),
        (c = !1),
        (d = function () {
          try {
            i.documentElement.doScroll("left");
          } catch (e) {
            return void setTimeout(d, 50);
          }
          l();
        })(),
        (i.onreadystatechange = function () {
          "complete" == i.readyState && ((i.onreadystatechange = null), l());
        }));
})(window);
