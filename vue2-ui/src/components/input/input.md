## 属性

type:     input类型

v-model： 双向数据绑定值

maxlength：最大长度

showWordLimit：是否显示数字限制角标

placeholder： 输入框占位文本

clearable： 是否显示可清空内容角标

showPassword： 是否显示密码切换

disabled: 是否禁用输入框

autosize： textarea内容高度设置，如{ minRows: 2, maxRows: 6 }

readonly： 是否只读


## 事件 event

focus:  在 Input 获得焦点时触发  (event: Event)

blur:   在 Input 失去焦点时触发   (event: Event)

input:  在 Input 值改变时触发     (value: string | number)

change: 仅在输入框失去焦点或用户按下回车时触发  (value: string | number)

clear:  在点击清空按钮时触发