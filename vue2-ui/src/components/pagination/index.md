## 全局引入

需要引入 input 组件

## 属性

background: 是否为分页按钮添加背景色（boolean）

page-size: 每页显示条目个数，支持 .sync 修饰符（number）

total: 总条目数（number）

page-count: 总页数，total 和 page-count 设置任意一个就可以达到显示页码的功能；(number)

pager-count: 页码按钮的数量，当总页数超过该值时会折叠。（number：大于等于 5 且小于等于 21 的奇数）

current-page: 当前页数，支持 .sync 修饰符。（number）

layout： 组件布局，子组件名用逗号分隔。（string: 可取值 sizes, prev, pager, next, jumper, total, slot）

prev-text: 替代图标显示的上一页文字（string）

next-text: 替代图标显示的下一页文字（string）

disabled: 是否禁用（boolean）

hide-on-single-page: 只有一页时是否隐藏（boolean）
