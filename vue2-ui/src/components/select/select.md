## 属性

v-model： 绑定值
disabled：是否禁用
placeHolder：占位符

## 事件

change: 选中 option 时发生变化时触发

## 子组件 option 属性

value： 选项值
label：选项的标签，若不设置则默认与 value 相同
disabled：是否禁用该选项
