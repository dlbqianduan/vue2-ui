import Vue from "vue";
import App from "./App.vue";
import "../src/assets/css/all.less";
import "../src/mock/mock";
import router from "./router";
import store from "./store";
import axios from "axios";
import message from "@/components/message/index.js";
import notification from "@/components/notification/index.js";
import MessageBox from "@/components/messageBox/index.js";
import loading from "@/components/loading/loading.js";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);

Vue.prototype.$message = message;

Vue.prototype.$notification = notification;

Vue.prototype.$messageBox = MessageBox;

Vue.prototype.$loading = loading;

Vue.config.productionTip = false;

Vue.config.ignoredElements = ["hhh", /^(dlb)/];

console.log(Vue.version);

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount("#app");
