import Mock from "mockjs";
import api from './api/index';
api.forEach(ele => {
    Mock.mock(ele.url, {
        'status': 1,
        'data':ele.response
    })
})
Mock.setup({
    timeout: '200-1000'
})