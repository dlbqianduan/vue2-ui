const gameList = [{
    name: '合成大西瓜',
    id: 1,
    url:'http://39.108.180.5/makeBigWaterMelon',
    tags: [
      {
        key: '0',
        label: '很好玩',
      }
    ]
  },{
    name: '五指棋',
    id: 2,
    url:'',
    tags: [
      {
        key: '0',
        label: '很好玩',
      }
    ]
  },{
    name: '合成大西瓜',
    id: 3,
    url:'http://39.108.180.5/makeBigWaterMelon',
    tags: [
      {
        key: '0',
        label: '很好玩',
      }
    ]
  }];
  
  export default [
    {
      url: '/api/gameList',
      method: 'get',
      response: gameList
    }
  ]