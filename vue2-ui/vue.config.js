const path = require("path")
module.exports = {
  devServer: {
    proxy: "https://jsonplaceholder.typicode.com",
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: [
        path.resolve(__dirname, "./src/assets/css/common/varible.less"),
      ],
    },
  },
}
